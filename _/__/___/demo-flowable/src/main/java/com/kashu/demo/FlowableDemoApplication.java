package com.kashu.demo;

import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class FlowableDemoApplication {
	/*
	    << all of codes copied from here: >>
	   https://www.canchito-dev.com/public/blog/2020/05/12/integrate-flowable-into-your-spring-boot-application/
	   https://springframework.guru/configuring-spring-boot-for-mariadb/
	
	    << available REST-API endpoints >>
	   POST    http://localhost:8081/process
	   GET     http://localhost:8081/tasks?assignee=kermit
	   GET     http://localhost:8081/actuator
	   GET     http://localhost:8081/actuator/flowable
	   GET     http://localhost:8081/actuator/health
	   
	*/
	public static void main(String[] args) {
		SpringApplication.run(FlowableDemoApplication.class, args);
	}
	
	@Bean
    public CommandLineRunner init(final RepositoryService repositoryService,
                                    final RuntimeService runtimeService,
                                    final TaskService taskService) {
        return new CommandLineRunner() {
            @Override
            public void run(String... strings) throws Exception {
                System.out.println("Number of process definitions : "
                    + repositoryService.createProcessDefinitionQuery().count());
                System.out.println("Number of tasks : " + taskService.createTaskQuery().count());
                runtimeService.startProcessInstanceByKey("oneTaskProcess");
                System.out.println("Number of tasks after process start: "
                    + taskService.createTaskQuery().count());
            }
        };
    }

}
