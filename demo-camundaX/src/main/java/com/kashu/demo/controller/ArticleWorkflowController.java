package com.kashu.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kashu.demo.domain.Approval;
import com.kashu.demo.domain.Article;
import com.kashu.demo.service.ArticleWorkflowService;

/*
 * All of codes were copied from here:
 * https://www.baeldung.com/flowable
 * 
 * 
 * HOW to call REST-API : 
 * 
 * step 1: The author submit a article 
 * POST    http://localhost:8084/submit
 * {
    "author":"tonyga",
    "url":"tw.yahoo.com"
    }
 * 
 * 
 * step 2: List all of the articles
 * GET     http://localhost:8084/tasks?assignee=editors
 * 
 * 
 * step 3: The Editors approve the publishing request
 * POST    http://localhost:8084/review
 * 
 * {
    "id": "bd125112-3dd3-11eb-8d22-024295bb2974",
    "status":true   
   }
 * 
 * 
 */


@RestController
public class ArticleWorkflowController {
    @Autowired
    private ArticleWorkflowService service;
    @PostMapping("/submit")
    public void submit(@RequestBody Article article) {
        service.startProcess(article);
    }
    @GetMapping("/tasks")
    public List<Article> getTasks(@RequestParam String assignee) {
        return service.getTasks(assignee);
    }
    @PostMapping("/review")
    public void review(@RequestBody Approval approval) {
        service.submitReview(approval);
    }
}
